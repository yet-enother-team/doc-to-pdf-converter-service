﻿using System;
using System.IO;
using Plossum;
using Plossum.CommandLine;

namespace Doc_to_PDF_Converter_Console_Client
{
    class Program
    {
        static string fullFileName;
        static string outputDirectory;

        static void Main(string[] args)
        {
            DocToPDFConverterService.Service1Client proxy = new DocToPDFConverterService.Service1Client();

            //If there are no errors in command line args
            if (ProcessCommandLineOptions(args) == 1)
            {
                byte[] byteArray = File.ReadAllBytes(fullFileName);
                string fileName = Path.GetFileNameWithoutExtension(fullFileName);
                string format = Path.GetExtension(fullFileName);
                byte[] incomingFile = proxy.UploadForConversion(byteArray, fileName, format);

                string savedFile;
                string newFormat;
                if (format == ".zip")
                {
                    newFormat = format;
                }
                else
                {
                    newFormat = ".pdf";
                }
                //If the output directory is user determined
                if(!String.IsNullOrEmpty(outputDirectory))
                {
                    savedFile = outputDirectory + @"\" + "new" + fileName + newFormat;
                }
                //If user has not entered the output directory
                else
                {
                    savedFile = Path.GetDirectoryName(fullFileName) + @"\" + "new" + fileName + newFormat;
                }    
                File.WriteAllBytes(savedFile, incomingFile);
            }
        }

        /// <summary>
        /// Process command line arguments using Plossum library 
        /// </summary>
        /// <param name="args"></param>
        /// <returns>System.Int32 1 if everything is fine</returns>
        static int ProcessCommandLineOptions(string[] args)
        {
            CommandLineOptions options = new CommandLineOptions();
            CommandLineParser parser = new CommandLineParser(options);
            parser.Parse();

            //If user invoked -H or gave no arguments
            if (options.H || args.Length == 0)
            {
                Console.WriteLine(parser.UsageInfo.GetOptionsAsString(78));
                return 0;
            }

            //If there are errors, -1 is returned
            else if (parser.HasErrors)
            {
                Console.WriteLine(parser.UsageInfo.GetErrorsAsString(78));
                return -1;
            }

            //If everything is good to go, return 1
            else
            {
                fullFileName = options.I;
                outputDirectory = options.O;
                return 1;
            }
        }
    }
}
