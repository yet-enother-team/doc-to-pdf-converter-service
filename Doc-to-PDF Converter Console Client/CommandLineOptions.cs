﻿using System;
using Plossum;
using Plossum.CommandLine;
using System.IO;

namespace Doc_to_PDF_Converter_Console_Client
{
    [CommandLineManager(ApplicationName = "\nParallel .DOC to .PDF Converter",
        Copyright = "Free Software by Aidil Umarov and Begimay Turdueva")]
    class CommandLineOptions
    {
        [CommandLineOption(Description = "Displays this help text")]
        public bool H = false;

        [CommandLineOption(Description = "Specify the .DOC file", MinOccurs = 1, MaxOccurs = 1)]
        public string I
        {
            get { return mInputFile; }
            set
            {
                if (String.IsNullOrEmpty(value))
                    throw new InvalidOptionValueException(
                        "You must specify the file you wish to convert", false);
                else if (!File.Exists(value))
                    throw new FileNotFoundException(
                        "File does not exist");
                else mInputFile = value;
            }
        }

        [CommandLineOption(Description = "Specify the output directory", MinOccurs = 0, MaxOccurs = 1)]
        public string O
        {
            get { return mOutputDirectory; }
            set
            {
                if (String.IsNullOrEmpty(value))
                    throw new InvalidOptionValueException(
                        "You must specify the directory to store the output file", false);
                else if (!Directory.Exists(value))
                    throw new DirectoryNotFoundException(
                        "Directory does not exist");
                else mOutputDirectory = value;
            }
        } 
        private string mInputFile;
        private string mOutputDirectory;
    }
}
