﻿using System.Threading.Tasks;
using System.IO;

namespace Doc_to_PDF_Converter_Service
{
    public class Service1 : IService1
    {
        private readonly string path = @"C:\temp";
        public byte[] UploadForConversion(byte[] byteArray, string fileName, string format)
        {
            IncomingFileManipulation process = new IncomingFileManipulation();

            //Step 1: Create a unique directory for each session
            string myPath = process.CreateUniqueDirectory(path);
            string fullFilePath = myPath + @"\" + fileName + format;

            //Step 2: Write incoming file into newly created directory
            process.SaveByteArrayToFile(byteArray, fullFilePath);

            //Step 3:
            //If the file is a zip containing docs
            if (format == ".zip")
            {
                //Extract files from the zip
                ZipManipulation zipManipulation = new ZipManipulation();
                zipManipulation.ExtractZip(fullFilePath, myPath);

                //Convert Word files
                string[] filesToConvert = process.GetAllFilesByExtension(myPath, "*.doc?");
                DocToPdfConversion conversion = new DocToPdfConversion();
                Parallel.ForEach<string>(filesToConvert, (file) =>
                {
                    conversion.SyncfusionConvertToPdf(file);
                });

                //Pack Files to zip
                string newFileName = myPath + "\\new" + fileName + ".zip";
                zipManipulation.PackToZip(myPath, newFileName, "*.pdf");
                byte[] returnFile = File.ReadAllBytes(newFileName);
                process.RemoveDirectoryAndContents(myPath);
                return returnFile;
                
            }
            
            //If the file is a doc or docx
            else if (format == ".doc" || format == ".docx")
            {
                //Convert Word file
                DocToPdfConversion conversion = new DocToPdfConversion();
                conversion.SyncfusionConvertToPdf(fullFilePath);
                byte[] returnFile = File.ReadAllBytes(myPath + @"\" + fileName + ".pdf");
                process.RemoveDirectoryAndContents(myPath);
                return returnFile;
            }

            //If the format is invalid
            else
            {
                return null;
            }
        }
    }
}
