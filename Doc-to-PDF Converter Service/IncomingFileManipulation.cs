﻿using System;
using System.IO;

namespace Doc_to_PDF_Converter_Service
{
    public class IncomingFileManipulation
    {
        public void SaveByteArrayToFile(byte[] byteArray, string fullFileName)
        {
            File.WriteAllBytes(fullFileName, byteArray);
        }

        public void RemoveDirectoryAndContents(string directoryPath)
        {
            DirectoryInfo di = new DirectoryInfo(directoryPath);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            Directory.Delete(directoryPath);
        }

        public string CreateUniqueDirectory(string path)
        {
            string uniqueFolderName = Guid.NewGuid().ToString();
            string pathstring = Path.Combine(path, uniqueFolderName);
            Directory.CreateDirectory(pathstring);
            return pathstring;
        }

        public string[] GetAllFilesByExtension(string folderPath, string nameExtension)
        {
            return Directory.GetFiles(folderPath, nameExtension);
        }

    }
}
