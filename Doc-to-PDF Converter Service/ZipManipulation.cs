﻿using System.IO;
using System.IO.Compression;

namespace Doc_to_PDF_Converter_Service
{
    class ZipManipulation
    {
        //pathFolder - the path of the folder to be zipped
        //pathZipFile -the path and name for the zip file to be created
        //the method CreateToZip creates archive pdf files using System.IO.CompressionZipArchive
        public void PackToZip(string pathFolder, string pathZipFile, string fileFormat)
        {
            using (ZipArchive zipArchive = ZipFile.Open(pathZipFile, ZipArchiveMode.Create))
            {
                DirectoryInfo di = new DirectoryInfo(pathFolder);
                FileInfo[] filesToArchive = di.GetFiles(fileFormat);
                if (filesToArchive != null && filesToArchive.Length > 0)
                {
                    foreach (FileInfo fileToArchive in filesToArchive)
                    {
                        zipArchive.CreateEntryFromFile(fileToArchive.FullName, fileToArchive.Name);
                    }
                }
            }
        }

        //pathZipFile - the path, where the zip file is located
        //destinationFolder - the path of the folder, where to extract files
        //the method ExtractToFiles extracts archive file using 
        //System.IO.Compression.ZipArchive object via the ZipFile.OpenRead method
        public void ExtractZip(string pathZipFile, string destinationFolder)
        {
            using (ZipArchive zipArchive = ZipFile.OpenRead(pathZipFile))
            {
                //get the entries in the zip file
                if (zipArchive.Entries != null && zipArchive.Entries.Count > 0)
                {
                    foreach (ZipArchiveEntry entry in zipArchive.Entries)
                    {
                        entry.ExtractToFile(System.IO.Path.Combine(destinationFolder, entry.FullName));
                    }
                }
            }
        }
    }
}
