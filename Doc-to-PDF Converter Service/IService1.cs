﻿using System.ServiceModel;

namespace Doc_to_PDF_Converter_Service
{
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        byte[] UploadForConversion(byte[] byteArray, string fileName, string format);
    }

}
