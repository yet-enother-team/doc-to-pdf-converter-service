﻿using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.Pdf;
using System.IO;

namespace Doc_to_PDF_Converter_Service
{
    class DocToPdfConversion
    {
        public void SyncfusionConvertToPdf(string file)
        {
            WordDocument wordDoc = new WordDocument(file, FormatType.Docx);
            DocToPDFConverter convertFile = new DocToPDFConverter();
            PdfDocument pdfDocument = convertFile.ConvertToPDF(wordDoc);
            convertFile.Dispose();
            wordDoc.Close();
            pdfDocument.Save(file.Replace(Path.GetExtension(file), ".pdf"));
            pdfDocument.Close(true);
        }
    }
}
